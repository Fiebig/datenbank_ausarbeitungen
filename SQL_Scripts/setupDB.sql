CREATE SCHEMA IF NOT EXISTS ausarbeitungen;
USE ausarbeitungen;

CREATE TABLE IF NOT EXISTS person (
    PersID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Vorname VARCHAR(20),
    Nachname VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS autor (
    Person_PersID INTEGER PRIMARY KEY,
    CONSTRAINT FOREIGN KEY (Person_PersID)
        REFERENCES person (PersID)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS betreuer (
    Person_PersID INTEGER PRIMARY KEY,
    CONSTRAINT FOREIGN KEY (Person_PersID)
        REFERENCES person (PersID)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS art_ausarbeitung (
    ArtID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Kuerzel VARCHAR(8) UNIQUE NOT NULL,
    ArtName VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS ausarbeitung (
    AusarbID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Datum DATE NOT NULL,
    Note FLOAT CHECK ((Note BETWEEN 1 AND 4) OR Note = 5),
    Titel VARCHAR(50) NOT NULL,
    Art_ArtID INTEGER,
    Erstautor_PersID INTEGER NOT NULL,
    CONSTRAINT FOREIGN KEY (Art_ArtID)
        REFERENCES art_ausarbeitung (ArtID)
        ON DELETE RESTRICT,
    CONSTRAINT FOREIGN KEY (Erstautor_PersID)
        REFERENCES autor (Person_PersID)
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS datei (
    DatID INTEGER PRIMARY KEY AUTO_INCREMENT,
    DatName VARCHAR(30) NOT NULL,
    Daten MEDIUMBLOB,
    Ausarbeitung_AusarbID INTEGER NOT NULL,
    CONSTRAINT FOREIGN KEY (Ausarbeitung_AusarbID)
        REFERENCES ausarbeitung (AusarbID)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ausarbeitung_autor (
    Autor_PersID INTEGER,
    Ausarbeitung_AusarbID INTEGER,
    PRIMARY KEY (Autor_PersID , Ausarbeitung_AusarbID),
    CONSTRAINT FOREIGN KEY (Autor_PersID)
        REFERENCES autor (Person_PersID) ON DELETE CASCADE,
	CONSTRAINT FOREIGN KEY (Ausarbeitung_AusarbID)
        REFERENCES ausarbeitung (AusarbID) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ausarbeitung_betreuer (
    Betreuer_PersID INTEGER,
    Ausarbeitung_AusarbID INTEGER,
    PRIMARY KEY (Betreuer_PersID , Ausarbeitung_AusarbID),
   CONSTRAINT FOREIGN KEY (Betreuer_PersID)
        REFERENCES betreuer (Person_PersID) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (Ausarbeitung_AusarbID)
        REFERENCES ausarbeitung (AusarbID) ON DELETE CASCADE
);



