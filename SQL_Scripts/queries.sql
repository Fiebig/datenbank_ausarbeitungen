use ausarbeitungen;

-- 1)
SELECT * FROM art_ausarbeitung;

-- 2)
SELECT 
    Titel
FROM
    ausarbeitung a
        JOIN
    person p ON (a.Erstautor_PersID = p.PersID)
WHERE
    Vorname = 'Donald' AND Nachname = 'Duck';
 
-- 3) 
SELECT 
    Titel
FROM
    ausarbeitung a
        JOIN
    ausarbeitung_betreuer ab ON (a.AusarbID = ab.Ausarbeitung_AusarbID)
        JOIN
    person p ON (ab.Betreuer_PersID = p.PersID)
WHERE
    Vorname = 'Daniel'
        AND Nachname = 'Düsentrieb';   
    
-- 4)   
SELECT 
    Titel
FROM
    ausarbeitung a
        LEFT OUTER JOIN
    ausarbeitung_betreuer ab ON (a.AusarbID = ab.Ausarbeitung_AusarbID)
WHERE
    NOT (a.AusarbID <=> ab.Ausarbeitung_AusarbID);
    
       
-- 5)
SELECT 
    Titel
FROM
    ausarbeitung a
        LEFT OUTER JOIN
    ausarbeitung_autor aa ON (a.AusarbID = aa.Ausarbeitung_AusarbID)
        JOIN
    person p ON (a.Erstautor_PersID = p.PersID
        OR aa.Autor_PersID <=> p.PersID)
WHERE
    Vorname = 'Donald' AND Nachname = 'Duck';
 
-- 6) 
SELECT DISTINCT
    Titel
FROM
    datei d
        JOIN
    ausarbeitung a ON (d.Ausarbeitung_AusarbID = a.AusarbID);
    
-- 7) 
SELECT 
    COUNT(AusarbID)
FROM
    ausarbeitung a
        JOIN
    art_ausarbeitung aa ON (a.Art_ArtID = aa.ArtID)
        JOIN
    ausarbeitung_betreuer ab ON (a.AusarbID = ab.Ausarbeitung_AusarbID)
        JOIN
    person p ON (ab.Betreuer_PersID = p.PersID)
WHERE
    Vorname = 'Daniel'
        AND Nachname = 'Düsentrieb'
        AND aa.ArtName = 'Seminararbeit';    
  
-- 8) 
SELECT 
    Vorname, nachname
FROM
    person p
        JOIN
    ausarbeitung a ON (p.PersID = a.Erstautor_PersID)
WHERE
    a.Note = (SELECT MIN(a.Note));
  
  
  
        
