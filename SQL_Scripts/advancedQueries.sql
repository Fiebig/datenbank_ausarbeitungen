use ausarbeitungen;

-- 1)
SELECT 
    CONCAT_WS(' ', Vorname, Nachname) AS Name
FROM
    person;
    
-- 2)    
SELECT 
    GROUP_CONCAT(CONCAT_WS(' ', Vorname, Nachname)
        SEPARATOR ', ') AS Alle_Betreuer
FROM
    person p
        JOIN
    ausarbeitung_betreuer ab ON (p.persID = ab.Betreuer_PersID)
GROUP BY ab.Ausarbeitung_AusarbID
HAVING ab.Ausarbeitung_AusarbID = 1;
   
-- 3)

CREATE OR REPLACE VIEW ausarbeitungen_view AS
    SELECT a.AusarbID,
        (SELECT 
                CONCAT_WS(' ', Vorname, Nachname)
            FROM
                person p
            WHERE
                p.persID = a.Erstautor_PersID) AS Autor,
        a.Titel,
        (SELECT 
                GROUP_CONCAT(CONCAT_WS(' ', p.Vorname, p.Nachname)
                        SEPARATOR ', ')
            FROM
                person p
            WHERE
                p.persid IN (SELECT 
                        ab2.Betreuer_PersID
                    FROM
                        ausarbeitung_betreuer ab2
                    WHERE
                        ab2.Ausarbeitung_AusarbID = ab.Ausarbeitung_AusarbID)) AS Betreuer,
        (SELECT 
                GROUP_CONCAT(CONCAT_WS(' ', p.Vorname, p.Nachname)
                        SEPARATOR ', ')
            FROM
                person p
            WHERE
                p.persid IN (SELECT 
                        aa2.Autor_PersID
                    FROM
                        ausarbeitung_autor aa2
                    WHERE
                        aa2.Ausarbeitung_AusarbID = aa.Ausarbeitung_AusarbID)) AS Weitere_Autoren
    FROM
        ausarbeitung a
            LEFT OUTER JOIN
        ausarbeitung_betreuer ab ON (a.AusarbID = ab.Ausarbeitung_AusarbID)
            LEFT OUTER JOIN
        ausarbeitung_autor aa ON (a.AusarbID = aa.Ausarbeitung_AusarbID)
    GROUP BY a.AusarbID;


SELECT 
    *
FROM
    ausarbeitungen_view;




