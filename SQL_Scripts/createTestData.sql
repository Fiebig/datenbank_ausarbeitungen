use ausarbeitungen;

Insert into art_ausarbeitung values(1,'S','Seminararbeit'),(2,'B','Bachelorarbeit'),(3,'M','Masterarbeit');
Insert into person values (1,'Dagobert','Duck'),(2,'Daniel','Düsentrieb'),(3,'Donald','Duck'),(4,'Daisy','Duck');
Insert into autor values (1),(2),(3),(4);
Insert into betreuer values(2),(3),(1);
Insert into ausarbeitung values(1,current_date,null,'Geld scheffeln',1,1),(2,current_date,1.3,'Busfahrplan Entenhausen',2,3),
(3,STR_TO_DATE('01.01.1990',"%d.%m.%Y"),null,'Erfinden für Dummies',3,2);
Insert into datei (DatID, DatName, Ausarbeitung_AusarbID) values (1,'ausarbeitung.pdf',3);
Insert into ausarbeitung_autor values (4,2),(3,3),(4,3);
Insert into ausarbeitung_betreuer values(2,1),(2,2),(3,1),(1,2),(1,3);
