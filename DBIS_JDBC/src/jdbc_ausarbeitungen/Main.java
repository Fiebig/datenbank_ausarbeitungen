package jdbc_ausarbeitungen;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
		
	public static void importSQLScript(Connection conn, InputStream in, String delimiterPattern) throws SQLException{
	    try(Scanner s = new Scanner(in);
	    		Statement st = conn.createStatement()){
		    s.useDelimiter(delimiterPattern);
		    //s.useDelimiter("(;(\r)?\n)|((\r)?\n)?(--)?.*(--(\r)?\n)");
	        while (s.hasNext())
	        {
	            String line = s.next();	        
	            if (line.trim().length() > 0){
	                st.addBatch(line);
	            }
	        }
	        st.executeBatch();
	    }
	}
	
	
	public static void main(String[] args) throws SQLException, IOException {
		if (args.length < 3) {
			System.out.println("Required arguments: ip:port user password");
			return;
		}
		try(Connection conn = DBConnector.createConnection(
				"jdbc:mysql://" + args[0] + "?useSSL=false", args[1], args[2]);
				FileInputStream fis1 = new FileInputStream("setupDB.sql");
				FileInputStream fis2 = new FileInputStream("createTrigger.sql"))
		{
				importSQLScript(conn, fis1, "(;(\r)?\n)|(--\n)");
				importSQLScript(conn, fis2, "((--)(\r)?\n)|(--\n)");
				new ConsoleApp(new DBAusarbeitungen(conn)).run();
		}
	}
}
