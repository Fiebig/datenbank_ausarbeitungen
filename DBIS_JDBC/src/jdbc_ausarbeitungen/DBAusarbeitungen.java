package jdbc_ausarbeitungen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBAusarbeitungen {

	private final Connection conn;
	
	public DBAusarbeitungen(Connection conn) throws SQLException {
		this.conn = conn;
		this.conn.setCatalog("ausarbeitungen");
	}

	public DBAusarbeitungen(String dbUrl, String dbUsername, String dbPassword) throws SQLException {
		this.conn = DBConnector.createConnection(dbUrl, dbUsername, dbPassword);
	}

	public boolean isConnected() {
		try {
			return !conn.isClosed();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void disConnect() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertDatei(int ausarbID, String dateiPfad) throws IOException, SQLException {
		File file = new File(dateiPfad);
		try (FileInputStream fis = new FileInputStream(file)) {
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			// write blob into DB
			try (PreparedStatement ps = conn
					.prepareStatement("INSERT INTO datei (DatName,Daten,Ausarbeitung_AusarbID) VALUES (?,?,?)")) {
				Blob b = conn.createBlob();
				b.setBytes(1, data);
				ps.setString(1, file.getName());
				ps.setBlob(2, b);
				ps.setInt(3, ausarbID);
				ps.executeUpdate();
			}
		}
	}

	public void writeDateienToDisk(int ausarbID, String directoryPfad) throws IOException, SQLException {
		try (PreparedStatement ps = conn
				.prepareStatement("SELECT datName, daten FROM datei d WHERE d.Ausarbeitung_AusarbID = ?")) {
			ps.setInt(1, ausarbID);
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				String datName = result.getString(1);
				Blob b = result.getBlob(2);
				byte[] data = b.getBytes(1, (int) b.length());
				// save blob to disk
				try (FileOutputStream fos = new FileOutputStream(new File(directoryPfad + File.separator + datName))) {
					fos.write(data);
					fos.flush();
				}
			}
		}
	}

	public void insertAusarbeitung(String vorName, String nachName, String titel, 
			String[] weitereAutoren, String[] betreuer) throws SQLException {
		conn.setAutoCommit(false);
		try {
			try (PreparedStatement ps = conn.prepareStatement(
					"Insert into ausarbeitung (Datum,Titel,Erstautor_PersID) values (current_date,?,?)",
					Statement.RETURN_GENERATED_KEYS)) {
				ps.setString(1, titel);
				ps.setInt(2, insertPersonIfNotExists(vorName, nachName, true, false));
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				try (PreparedStatement ps2 = conn.prepareStatement("Insert into ausarbeitung_autor VALUES (?,?)");
						PreparedStatement ps3 = conn.prepareStatement("Insert into ausarbeitung_betreuer VALUES (?,?)")) {
					rs.next();
					int ausarbeitung_AusarbID = rs.getInt(1);
					for (int i = 0; i < weitereAutoren.length; i += 2) {
						ps2.setInt(2, ausarbeitung_AusarbID);
						ps2.setInt(1, insertPersonIfNotExists(weitereAutoren[i], weitereAutoren[i + 1], true, false));
						ps2.executeUpdate();
					}
					for (int i = 0; i < betreuer.length; i += 2) {
						ps3.setInt(2, ausarbeitung_AusarbID);
						ps3.setInt(1, insertPersonIfNotExists(betreuer[i], betreuer[i + 1], false, true));
						ps3.executeUpdate();
					}
				}
				conn.commit();
			} catch (SQLException e) {
				conn.rollback();
				throw e;
			}
		} finally {
			conn.setAutoCommit(true);
		}
	}

	public int insertPersonIfNotExists(String vorName, String nachName, boolean autor, boolean betreuer)
			throws SQLException {
		try (PreparedStatement ps = conn
				.prepareStatement("SELECT p.PersID FROM person p WHERE p.vorname = ? AND p.nachname = ?")) {
			ps.setString(1, vorName);
			ps.setString(2, nachName);
			ResultSet result = ps.executeQuery();
			try (PreparedStatement ps2 = conn.prepareStatement("INSERT INTO person (vorname,nachname) VALUES (?,?)",
					Statement.RETURN_GENERATED_KEYS)) {
				int persID;
				if (result.next()) {
					persID = result.getInt(1);
				} else {
					ps2.setString(1, vorName);
					ps2.setString(2, nachName);
					ps2.executeUpdate();
					ResultSet result2 = ps2.getGeneratedKeys();
					result2.next();
					persID = result2.getInt(1);
				}
				if (autor) {
					insertAutorIfNotExists(persID);
				}
				if (betreuer) {
					insertBetreuerIfNotExists(persID);
				}
				return persID;
			}
		}
	}

	private void insertAutorIfNotExists(int persID) throws SQLException {
		try (PreparedStatement ps = conn
				.prepareStatement("SELECT a.Person_PersID FROM autor a WHERE a.Person_PersID = ?")) {
			ps.setInt(1, persID);
			ResultSet result = ps.executeQuery();
			if (!result.next()) {
				try (PreparedStatement ps2 = conn.prepareStatement("INSERT INTO autor (Person_PersID) VALUES (?)")) {
					ps2.setInt(1, persID);
					ps2.executeUpdate();
				}
			}
		}
	}

	private void insertBetreuerIfNotExists(int persID) throws SQLException {
		try (PreparedStatement ps = conn
				.prepareStatement("SELECT b.Person_PersID FROM betreuer b WHERE b.Person_PersID = ?")) {
			ps.setInt(1, persID);
			ResultSet result = ps.executeQuery();
			if (!result.next()) {
				try (PreparedStatement ps2 = conn.prepareStatement("INSERT INTO betreuer (Person_PersID) VALUES (?)")) {
					ps2.setInt(1, persID);
					ps2.executeUpdate();
				}
			}
		}
	}

	public void insertArt(String kuerzel, String artName) throws SQLException {
		try (PreparedStatement ps = conn
				.prepareStatement("INSERT INTO art_ausarbeitung (Kuerzel,ArtName) VALUES (?,?)")) {
			ps.setString(1, kuerzel);
			ps.setString(2, artName);
			ps.executeUpdate();
		}
	}

	public void deleteArt(String kuerzel) throws SQLException {
		try (PreparedStatement ps = conn.prepareStatement("DELETE FROM art_ausarbeitung WHERE Kuerzel = ?")) {
			ps.setString(1, kuerzel);
			ps.executeUpdate();
		}
	}

	public void deleteArt(int artID) throws SQLException {
		try (PreparedStatement ps = conn.prepareStatement("DELETE FROM art_ausarbeitung WHERE ArtID  = ?")) {
			ps.setInt(1, artID);
			ps.executeUpdate();
		}
	}
	
	public void deleteAusarbeitung(int ausarbID) throws SQLException {
		try (PreparedStatement ps = conn.prepareStatement("DELETE FROM ausarbeitung WHERE AusarbID  = ?")) {
			ps.setInt(1, ausarbID);
			ps.executeUpdate();
		}
	}

	public String getDateienAsString(int ausarbID) throws SQLException {
		try (PreparedStatement ps = conn
				.prepareStatement("SELECT DatID,DatName FROM datei d WHERE d.Ausarbeitung_AusarbID = ?")) {
			ps.setInt(1, ausarbID);
			ResultSet result = ps.executeQuery();
			StringBuilder sb = new StringBuilder(String.format("%-10s%-25s%n", "DatID", "|DatName"));
			sb.append(new String(new char[30]).replace('\0', '-') + "\n");
			while (result.next()) {
				int datID = result.getInt(1);
				String datName = result.getString(2);
				sb.append(String.format("%-10s%-25s%n", datID, "|" + datName));
			}
			return sb.toString();
		}
	}

	public String getArtenAsString() throws SQLException {
		try (Statement s = conn.createStatement()) {
			ResultSet result = s.executeQuery("SELECT Kuerzel,ArtName FROM art_ausarbeitung");

			StringBuilder sb = new StringBuilder(String.format("%-10s%-25s%n", "Kuerzel", "|ArtName"));
			sb.append(new String(new char[30]).replace('\0', '-') + "\n");
			while (result.next()) {
				String kuerzel = result.getString(1);
				String artName = result.getString(2);
				sb.append(String.format("%-10s%-25s%n", kuerzel, "|" + artName));
			}
			return sb.toString();
		}
	}

	public String getAusarbeitungenAsString() throws SQLException {
		try (Statement s = conn.createStatement()) {
			ResultSet result = s
					.executeQuery("SELECT ausarbid,autor,titel,betreuer,weitere_autoren FROM ausarbeitungen_view");
			StringBuilder sb = new StringBuilder(String.format("%-10s%-25s%-30s%-50s%-50s%n", "AusarbID", "|Autor",
					"|Titel", "|Betreuer", "|Weitere_Autoren"));
			sb.append(new String(new char[150]).replace('\0', '-') + "\n");
			while (result.next()) {
				int ausarbID = result.getInt(1);
				String autor = result.getString(2);
				String titel = result.getString(3);
				String betreuer = result.getString(4);
				String weitereAutoren = result.getString(5);
				sb.append(String.format("%-10s%-25s%-30s%-50s%-50s%n", ausarbID, "|" + autor, "|" + titel,
						"|" + betreuer, "|" + weitereAutoren));
			}
			return sb.toString();
		}
	}

}
