package jdbc_ausarbeitungen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {

	public static Connection createConnection(String dbUrl, String dbUsername, String dbPassword) throws SQLException {
		return DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
	}

	/*static {
		try {
			Class.forName("org.mariadb.jdbc.Driver").newInstance();
		}
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new RuntimeException("Fehler beim Laden des DriverManager!", e);
		}
	}*/

}
