package jdbc_ausarbeitungen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleApp {

	private final DBAusarbeitungen dbAusarbeitungen;

	public ConsoleApp(DBAusarbeitungen dbAusarbeitungen) {
		this.dbAusarbeitungen = dbAusarbeitungen;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		String n = System.getProperty("line.separator");
		sc.useDelimiter("[,\t" + n + " ]+");
		boolean shouldRun = true;
		while (shouldRun) {
			try {
				printMenu();
				System.out.println("Bitte Menupunkt waehlen:");
				int input = sc.nextInt();
				switch (input) {
				case 1:
					System.out.println(dbAusarbeitungen.getArtenAsString());
					break;
				case 2:
					System.out.println("Bitte Kuerzel und ArtName eingeben:");
					dbAusarbeitungen.insertArt(sc.next(), sc.next());
					break;
				case 3:
					System.out.println("Bitte Kuerzel eingeben:");
					dbAusarbeitungen.deleteArt(sc.next());
					break;
				case 4:
					System.out.println(dbAusarbeitungen.getAusarbeitungenAsString());
					break;
				case 5:
					case5(sc);
					break;
				case 6:
					System.out.println("Bitte AusarbeitungsID eingeben:");
					dbAusarbeitungen.deleteAusarbeitung(sc.nextInt());
					break;
				case 7:
					System.out.println("Bitte AusarbeitungsID und Quellpfad eingeben:");
					dbAusarbeitungen.insertDatei(sc.nextInt(), sc.next());
					break;
				case 8:
					System.out.println("Bitte AusarbeitungsID eingeben:");
					System.out.println(dbAusarbeitungen.getDateienAsString(sc.nextInt()));
					break;
				case 9:
					System.out.println("Bitte AusarbeitungsID und Zielpfad eingeben:");
					dbAusarbeitungen.writeDateienToDisk(sc.nextInt(), sc.next());
					break;
				case 0:
					shouldRun = false;
					break;
				default:
					System.err.println("Eingabe muss Ganzzahl zwischen 0 und 9 sein!");
					continue;
				}
				System.out.println("Operation erfolgreich.");
			}
			catch (InputMismatchException e) {
				System.err.println("Eingabe keine Ganzzahl!");
				sc.nextLine();
			}
			catch (IOException | SQLException e) {
				System.err.println("Operation fehlgeschlagen: " + e.getMessage());
			}
		}
		sc.close();
	}

	private void case5(Scanner sc) throws SQLException {
		System.out.println("Bitte Vorname, Nachname, Titel, Anzahl weiterer Autoren und Anzahl Betreuer eingeben:");
		String vn = sc.next(), nn = sc.next(), t = sc.next();
		int anzWa = sc.nextInt();
		int anzBetr = sc.nextInt();
			if (anzWa > 0)
				System.out.println("Bitte Vorname und Nachname der weiteren Autoren eingeben:");
			String[] wa = new String[anzWa * 2];
			for (int i = 0; i < anzWa * 2; i++) {
				wa[i] = sc.next();
			}
			if (anzBetr > 0)
				System.out.println("Bitte Vorname und Nachname der Betreuer eingeben:");
			String[] betr = new String[anzBetr * 2];
			for (int i = 0; i < anzBetr * 2; i++) {
				betr[i] = sc.next();
			}
			dbAusarbeitungen.insertAusarbeitung(vn, nn, t, wa, betr);
	}

	private void printMenu() {
		String menu = "\nHauptmenu\n" + "1 - Alle Arten ausgeben\n" + "2 - Eine neue Art einfuegen\n"
				+ "3 - Eine Art loeschen\n" + "4 - Alle Ausarbeitungen ausgeben\n"
				+ "5 - Eine neue Ausarbeitung einfuegen\n" + "6 - Eine Ausarbeitung loeschen\n"
				+ "7 - Datei zu Ausarbeitung hinzufuegen\n" + "8 - Dateien von Ausarbeitung ausgeben\n"
				+ "9 - Datei auf Festplatte schreiben\n" + "0 - Beenden\n";
		System.out.println(menu);
	}

}
