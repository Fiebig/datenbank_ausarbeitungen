use ausarbeitungen;
--

/* instert + update trigger */

CREATE TRIGGER IF NOT EXISTS datNameCheckI
BEFORE INSERT ON datei
FOR EACH ROW
BEGIN
IF EXISTS (Select DatID FROM datei d where d.DatName = New.DatName AND d.Ausarbeitung_AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Gleiche Dateinamen bei gleichen Ausarbeitungen nicht erlaubt!';
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS datNameCheckU
BEFORE UPDATE ON datei
FOR EACH ROW
BEGIN
IF EXISTS (Select DatID FROM datei d where d.DatName = New.DatName AND d.Ausarbeitung_AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Gleiche Dateinamen bei gleichen Ausarbeitungen nicht erlaubt!';
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS autorConsistencyI
BEFORE INSERT ON ausarbeitung_autor
FOR EACH ROW
BEGIN
IF EXISTS (Select * FROM ausarbeitung_betreuer ab
WHERE ab.Betreuer_PersID = New.Autor_PersID AND ab.Ausarbeitung_AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Person darf nicht gleichzeitig Betreuer und Autor derselben Ausarbeitung sein!';
END IF;
IF EXISTS (Select * FROM ausarbeitung a
WHERE a.Erstautor_PersID = New.Autor_PersID AND a.AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Person darf nicht mehrmals als Autor der gleichen Ausarbeitung auftreten!';
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS autorConsistencyU
BEFORE UPDATE ON ausarbeitung_autor
FOR EACH ROW
BEGIN
IF EXISTS (Select * FROM ausarbeitung_betreuer ab
WHERE ab.Betreuer_PersID = New.Autor_PersID AND ab.Ausarbeitung_AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Person darf nicht gleichzeitig Betreuer und Autor derselben Ausarbeitung sein!';
END IF;
IF EXISTS (Select * FROM ausarbeitung a
WHERE a.Erstautor_PersID = New.Autor_PersID AND a.AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Person darf nicht mehrmals als Autor der gleichen Ausarbeitung auftreten!';
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS betreuerConsistencyI
BEFORE INSERT ON ausarbeitung_betreuer
FOR EACH ROW
BEGIN
IF EXISTS (Select Ausarbeitung_AusarbID, Autor_PersID FROM ausarbeitung_autor aa
WHERE aa.Autor_PersID = New.Betreuer_PersID AND aa.Ausarbeitung_AusarbID = New.Ausarbeitung_AusarbID
UNION ALL
Select AusarbID, Erstautor_PersID From ausarbeitung a 
WHERE a.Erstautor_PersID = New.Betreuer_PersID AND a.AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Person darf nicht gleichzeitig Betreuer und Autor derselben Ausarbeitung sein!';
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS betreuerConsistencyU
BEFORE UPDATE ON ausarbeitung_betreuer
FOR EACH ROW
BEGIN
IF EXISTS (Select Ausarbeitung_AusarbID, Autor_PersID FROM ausarbeitung_autor aa
WHERE aa.Autor_PersID = New.Betreuer_PersID AND aa.Ausarbeitung_AusarbID = New.Ausarbeitung_AusarbID
UNION ALL
Select AusarbID, Erstautor_PersID From ausarbeitung a 
WHERE a.Erstautor_PersID = New.Betreuer_PersID AND a.AusarbID = New.Ausarbeitung_AusarbID) THEN
SIGNAL SQLSTATE '45000' SET message_text = 'Person darf nicht gleichzeitig Betreuer und Autor derselben Ausarbeitung sein!';
END IF;
END;
--

/* procedures */

CREATE PROCEDURE IF NOT EXISTS isAutorReferenced(IN Autor_PersID INTEGER,OUT boolResult BOOLEAN)
BEGIN
SELECT EXISTS (Select Autor_PersID FROM ausarbeitung_autor aa
WHERE aa.Autor_PersID = Autor_PersID
UNION ALL
Select Erstautor_PersID From ausarbeitung a 
WHERE a.Erstautor_PersID = Autor_PersID) INTO boolResult;
END;
--


CREATE PROCEDURE IF NOT EXISTS isBetreuerReferenced(IN Betreuer_PersID INTEGER,OUT boolResult BOOLEAN)
BEGIN
SELECT EXISTS (Select Betreuer_PersID FROM ausarbeitung_betreuer ab
WHERE ab.Betreuer_PersID = Betreuer_PersID) INTO boolResult;
END;
--

/* delete trigger */

CREATE TRIGGER IF NOT EXISTS autor_loeschen1_D
AFTER DELETE ON ausarbeitung
FOR EACH ROW
BEGIN
CALL isAutorReferenced(Old.Erstautor_PersID,@boolResult1);
IF (SELECT @boolResult1 = false) THEN
DELETE FROM autor WHERE Person_PersID = OLD.Erstautor_PersID;
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS autor_loeschen2_D
AFTER DELETE ON ausarbeitung_autor
FOR EACH ROW
BEGIN
CALL isAutorReferenced(Old.Autor_PersID,@boolResult1);
IF (SELECT @boolResult1 = false) THEN
DELETE FROM autor WHERE Person_PersID = OLD.Autor_PersID;
END IF;
END;
--


CREATE TRIGGER IF NOT EXISTS betreuer_loeschen1_D
AFTER DELETE ON ausarbeitung_betreuer
FOR EACH ROW
BEGIN
CALL isBetreuerReferenced(Old.Betreuer_PersID,@boolResult);
IF (SELECT @boolResult = false) THEN
DELETE FROM betreuer WHERE Person_PersID = OLD.Betreuer_PersID;
END IF;
END;
--

