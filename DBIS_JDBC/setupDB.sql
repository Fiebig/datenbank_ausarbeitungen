-- create db

CREATE SCHEMA IF NOT EXISTS ausarbeitungen;
USE ausarbeitungen;

-- create tables

CREATE TABLE IF NOT EXISTS person (
    PersID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Vorname VARCHAR(20),
    Nachname VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS autor (
    Person_PersID INTEGER PRIMARY KEY,
    CONSTRAINT FOREIGN KEY (Person_PersID)
        REFERENCES person (PersID)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS betreuer (
    Person_PersID INTEGER PRIMARY KEY,
    CONSTRAINT FOREIGN KEY (Person_PersID)
        REFERENCES person (PersID)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS art_ausarbeitung (
    ArtID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Kuerzel VARCHAR(8) UNIQUE NOT NULL,
    ArtName VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS ausarbeitung (
    AusarbID INTEGER PRIMARY KEY AUTO_INCREMENT,
    Datum DATE NOT NULL,
    Note FLOAT CHECK ((Note BETWEEN 1 AND 4) OR Note = 5),
    Titel VARCHAR(50) NOT NULL,
    Art_ArtID INTEGER,
    Erstautor_PersID INTEGER NOT NULL,
    CONSTRAINT FOREIGN KEY (Art_ArtID)
        REFERENCES art_ausarbeitung (ArtID)
        ON DELETE RESTRICT,
    CONSTRAINT FOREIGN KEY (Erstautor_PersID)
        REFERENCES autor (Person_PersID)
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS datei (
    DatID INTEGER PRIMARY KEY AUTO_INCREMENT,
    DatName VARCHAR(30) NOT NULL,
    Daten MEDIUMBLOB,
    Ausarbeitung_AusarbID INTEGER NOT NULL,
    CONSTRAINT FOREIGN KEY (Ausarbeitung_AusarbID)
        REFERENCES ausarbeitung (AusarbID)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ausarbeitung_autor (
    Autor_PersID INTEGER,
    Ausarbeitung_AusarbID INTEGER,
    PRIMARY KEY (Autor_PersID , Ausarbeitung_AusarbID),
    CONSTRAINT FOREIGN KEY (Autor_PersID)
        REFERENCES autor (Person_PersID) ON DELETE CASCADE,
	CONSTRAINT FOREIGN KEY (Ausarbeitung_AusarbID)
        REFERENCES ausarbeitung (AusarbID) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ausarbeitung_betreuer (
    Betreuer_PersID INTEGER,
    Ausarbeitung_AusarbID INTEGER,
    PRIMARY KEY (Betreuer_PersID , Ausarbeitung_AusarbID),
   CONSTRAINT FOREIGN KEY (Betreuer_PersID)
        REFERENCES betreuer (Person_PersID) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (Ausarbeitung_AusarbID)
        REFERENCES ausarbeitung (AusarbID) ON DELETE CASCADE
);

-- create view

CREATE OR REPLACE VIEW ausarbeitungen_view AS
    SELECT a.AusarbID,
        (SELECT 
                CONCAT_WS(' ', Vorname, Nachname)
            FROM
                person p
            WHERE
                p.persID = a.Erstautor_PersID) AS Autor,
        a.Titel,
        (SELECT 
                GROUP_CONCAT(CONCAT_WS(' ', p.Vorname, p.Nachname)
                        SEPARATOR ', ')
            FROM
                person p
            WHERE
                p.persid IN (SELECT 
                        ab2.Betreuer_PersID
                    FROM
                        ausarbeitung_betreuer ab2
                    WHERE
                        ab2.Ausarbeitung_AusarbID = ab.Ausarbeitung_AusarbID)) AS Betreuer,
        (SELECT 
                GROUP_CONCAT(CONCAT_WS(' ', p.Vorname, p.Nachname)
                        SEPARATOR ', ')
            FROM
                person p
            WHERE
                p.persid IN (SELECT 
                        aa2.Autor_PersID
                    FROM
                        ausarbeitung_autor aa2
                    WHERE
                        aa2.Ausarbeitung_AusarbID = aa.Ausarbeitung_AusarbID)) AS Weitere_Autoren
    FROM
        ausarbeitung a
            LEFT OUTER JOIN
        ausarbeitung_betreuer ab ON (a.AusarbID = ab.Ausarbeitung_AusarbID)
            LEFT OUTER JOIN
        ausarbeitung_autor aa ON (a.AusarbID = aa.Ausarbeitung_AusarbID)
    GROUP BY a.AusarbID;

